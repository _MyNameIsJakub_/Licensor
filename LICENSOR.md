Copyright (c) 2021 {firstName lastName/legalEntityName} (hereinafter referred to as a „Licensor“). All rights reserved.

Licensor grants you a irrevocable, non-exclusive, non-sublicensable, royalty-free and worldwide license to its software (hereinafter referred to as a „Software“).

Licensees (hereinafter referred to as „You“) are free to deal with this Software under the following terms:

1. You can use, copy, modify, merge and publish this Software and distribute copies of it.
2. You may not use this Software for commercial purposes.
3. You are obligated to give credit to the Licensor.
4. You are obligated to distribute all your contributions to this Software under the same terms as this (original) Software.
5. You are obligated to include the content of this file (LICENSE.md) in all copies of this Software.

This Software is distributed to You by the Licensor „as is“ with no warranties.

Licensor disclaims all liability of any kind that arises out from dealing with this Software.

*Licensees can obtain an additional license to use this Software for commercial purposes by contacting the Licensor at {[local-part@domain.tld](local-part@domain.tld)}.*
