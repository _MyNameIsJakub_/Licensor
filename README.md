# Licensor

Licensor is an MIT License inspired software license that prohibits commercial use of your work.

Text of this license is published under the [CC0 1.0 Universal (CC0 1.0)](https://creativecommons.org/publicdomain/zero/1.0/deed.en) license.

Feel free to contact me at [hello@jakubpejzl.me](mailto:hello@jakubpejzl.me) with any questions related to Licensor.
